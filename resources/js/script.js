function fadedEls(el, shift) {
	el.css('opacity', 0);

	switch (shift) {
	case undefined:
		shift = 0;
		break;
	case 'h':
		shift = el.eq(0).outerHeight();
		break;
	case 'h/2':
		shift = el.eq(0).outerHeight() / 2;
		break;
	}

	$(window)
			.resize(
					function() {
						if (!el.hasClass('ani-processed')) {
							el.eq(0).data(
									'scrollPos',
									el.eq(0).offset().top - $(window).height()
											+ shift);
						}
					}).scroll(function() {
				if (!el.hasClass('ani-processed')) {
					if ($(window).scrollTop() >= el.eq(0).data('scrollPos')) {
						el.addClass('ani-processed');
						el.each(function(idx) {
							$(this).delay(idx * 200).animate({
								opacity : 1
							}, 600);
						});
					}
				}
			});
}

if (/Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent)) {
	window.mobile = true;
} else {
	window.mobile = false;
}

(function($) {
	$(function() {

		// Focus state for append/prepend inputs
		$('.input-prepend, .input-append').on('focus', 'input', function() {
			$(this).closest('.control-group, form').addClass('focus');
		}).on('blur', 'input', function() {
			$(this).closest('.control-group, form').removeClass('focus');
		});

		// features ani
		fadedEls($('.features').parent().find('h3'), 'h');
		$('.features > *').each(function() {
			fadedEls($(this), 150);
		});

		// responsive
		$(window)
				.resize(
						function() {
							// input-append auto width
							$('footer .input-append input[type="text"]')
									.each(
											function() {
												var controlGroup = $(this)
														.closest(
																'.control-group');

												if ($(window).width() > 480) {
													$(this).css('width', '');
												} else {
													$(this)
															.css(
																	'width',
																	controlGroup
																			.outerWidth()
																			- controlGroup
																					.find(
																							'.input-append .btn')
																					.outerWidth());
												}
											});

							// social-btns
							if ($(window).width() > 480) {
								$('footer .social-btns.mobile-processed')
										.removeClass('mobile-processed')
										.appendTo(
												'footer > .container > .row > .col-sm-3:last');
							} else {
								$('footer .social-btns:not(.mobile-processed)')
										.addClass('mobile-processed')
										.insertBefore('footer nav');
							}
						});

		$(window).resize().scroll();

	});
})(jQuery);

function validarfecha(valor) {
	valor = new Date(document.getElementById("f_rangeEnd").value);
	var d = new Date();
	var fechaAhora = new Date(d.getFullYear() + "/" + (d.getMonth() + 1) + "/"
			+ d.getDate());
	var dif = fechaAhora - valor;
	var fechafin = dif / 1000 / 60 / 60 / 24 / 365;
	if (fechafin < 16) {
		document.getElementById('checkmenor').checked = true;
	} else {
		document.getElementById('checkmenor').checked = false;
	}
	// alert("years: "+fechafin);
}

function validarFormularioConsulta() {
	var documentacion = document.getElementById("documento");
	if (documentacion.value.length == 0) {
		alert("El documento no puede estar vacio...");
		return false;
	} else if (!validaNif(documentacion.value) && !validaNie(documentacion.value)) {
		alert("El documento no es valido...");
		return false;
	}
}

function validarFormulario() {
	var documentacion = document.getElementById("documento");
	var fecha = document.getElementById("fecha_nacimiento");
	var email = document.getElementById("email");
	var check_menor = document.getElementById("checkTutor");
	var check_consorcio = document.getElementById("checkConsorcio");
	if (documentacion.value.length == 0) {
		alert("El documento no puede estar vacio...");
		return false;
	} else if (!validaNif(documentacion.value) && !validaNie(documentacion.value)) {
		alert("El documento no es valido...");
		return false;
	} else if (!validarFormatoFecha(fecha.value)) {
		return false;
	// Validamos que el usuario
	} else if (email.value.length == 0) {
		alert("El email no puede estar vacio...");
		return false;
	} else if (!validateEmail(email.value)) {
		alert("El formato del email no es correcto...");
		return false;
	} else if(isMenorDe(16,fecha.value) && isMayorDe(14,fecha.value)) {
		if (!check_menor.checked) {
			check_menor.checked=true;
			alert("El usuario tiene entre 14 y 16 años. Rellene los campos del tutor.");
			return false;
		} else {
			var doc_tutor = document.getElementById("doc_tutor");
			var nombre_tutor = document.getElementById("nombre_tutor");
			var apellido1_tutor = document.getElementById("apellido1_tutor");
			if (doc_tutor.value.length == 0) {
				alert("El documento del tutor no puede estar vacio...");
				return false;
			} else if (doc_tutor.value == documentacion.value) {
				alert("El documento del tutor no puede ser el mismo que el del usuario...");
				return false;
			} else if (!validaNif(doc_tutor.value) && !validaNie(doc_tutor.value)) {
				alert("El documento del tutor no es válido...");
				return false;
			} else if (documentacion.value == doc_tutor.value) {
				alert("El documento del tutor no puede ser el mismo que el del usuario...");
				return false;
			} else if (nombre_tutor.value.length == 0) {
				alert("El nombre del tutor no puede estar vacio...");
				return false;
			} else if (apellido1_tutor.value.length == 0) {
				alert("El primer apellido del tutor no puede estar vacio...");
				return false;
			}
		}
	} else if (isMenorDe(14,fecha.value))	{
			alert("Un usuario menor de 14 años no se puede registrar.");
			return false;

	} else if (check_consorcio.checked) {
		var tarjeta_transporte = document.getElementById("tarjeta_transporte");
		if (tarjeta_transporte.value.length == 0) {
			alert("Debe rellenar el número de tarjeta de transporte");
			return false;
		} else if (validateNumbers(tarjeta_transporte.value)) {
			alert("Debe introducir solamente números");
			return false;
		} else if (tarjeta_transporte.value.length != 22) {
			alert("El número de tarjeta de transporte debe ser de 22 dígitos");
			return false;
		} 
	} else {
		return true;
	}

}

function validarFormularioCompraAbono() {
	var check_condiciones = document.getElementById("checkcondiciones");
	var check_edad = document.getElementById("checkmenores");
	
	if (!check_condiciones.checked) {
		alert("Debe aceptar las instrucciones de uso.");
		return false;
	}
	
	if (!check_edad.checked) {
		alert("Debe confirmar que tiene más de 16 años.");
		return false;
	}
	
	return true;
}

function validarFormularioBajaUsuario() {
	var documento = document.getElementById("documento");
	
	if (documento.value.length == 0) {
		alert("El documento no puede estar vacio...");
		return false;
	}
	return true;
}

function validarFormatoFecha(fecha) {
	var RegExPattern = /^\d{1,2}\/\d{1,2}\/\d{2,4}$/;

	if ((fecha.match(RegExPattern)) && (fecha != '')) {
		return true;
	} else {
		alert("Formato de fecha incorrecto");
		return false;
	}
}


function validarFecha(fecha)
{
	var check_menor = document.getElementById('checkTutor');
	if(isMenorDe(16,fecha) && isMayorDe(14,fecha) && !check_menor.checked)
	{
		check_menor.checked=true;
		alert("El usuario tiene entre 14 y 16 años. Debe tener un tutor.");
		return false;
	} else if (isMenorDe(14,fecha))
	{
		alert("Un usuario menor de 14 años no se puede registrar.");
		return false;
	}
	else
	{
		check_menor.checked=false;
		return true;
	}

}

function isMenorDe(edadPermitida, fecha)
{
	var fechaf = fecha.split("/");
	var day = fechaf[0];
	var month = fechaf[1];
	var year = fechaf[2];
	
	var now=new Date();
	var currentYear = now.getFullYear();
	var currentMonth = now.getMonth() + 1;
	var currentDay = now.getDay();
	if (currentYear - year < edadPermitida)
	{
		resultado = true;
	} else if (currentYear - year == edadPermitida) {
		if (currentMonth < month) {
			resultado = true;
		} else if (currentMonth == month)
		{
			if (currentDay < day)
			{
				resultado = true;
			} else {
				resultado = false;
			}
		} else {
			resultado = false;
		}
	} else {
		resultado = false;
	}
	
	return resultado;
}

function isMayorDe(edadPermitida, fecha)
{
	var fechaf = fecha.split("/");
	var day = fechaf[0];
	var month = fechaf[1];
	var year = fechaf[2];
	
	var now=new Date();
	var currentYear = now.getFullYear();
	var currentMonth = now.getMonth() + 1;
	var currentDay = now.getDay();
	if (currentYear - year > edadPermitida)
	{
		resultado = true;
	} else if (currentYear - year == edadPermitida) {
		if (currentMonth > month) {
			resultado = true;
		} else if (currentMonth == month)
		{
			if (currentDay >= day)
			{
				resultado = true;
			} else {
				resultado = false;
			}
		} else {
			resultado = false;
		}
	} else {
		resultado = false;
	}
	
	return resultado;
}

function aMayusculas(obj, id) {
	obj = obj.toUpperCase();
	document.getElementById(id).value = obj;
}

function validaNif(value) {
	value = value.toUpperCase();

	// Comprobamos el formato
	if (!value
			.match('((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)')) {
		return false;
	}

	// Validamos la letra del NIF
	if (/^[0-9]{8}[A-Z]{1}$/.test(value)) {
		return ("TRWAGMYFPDXBNJZSQVHLCKE".charAt(value.substring(8, 0) % 23) === value
				.charAt(8));
	}

	// Letras especiales (Inicio K, L o M)
	if (/^[KLM]{1}/.test(value)) {
		digits = value.substring(1, 8);
		sumaImpar = 0;
		sumaPar = 0;

		for (cont = 0; cont < digits.length; cont = cont + 2) {
			if (cont < 6) {
				sumaImpar += 1 * digits[cont + 1];
			}
			dobleImpar = 2 * digits[cont];
			sumaPar += Math.floor((dobleImpar % 10) + (dobleImpar / 10));
		}

		sumaTotal = sumaPar + sumaImpar;
		sumaTotal = Math.floor((10 - (sumaTotal % 10)) % 10);

		return (value.charAt(8) === "JABCDEFGHI".charAt(sumaTotal));
	}

	return false;
}

function validaNie(value) {
	value = value.toUpperCase();

	// Validamos si la estructura es correcta
	if (!value
			.match('((^[A-Z]{1}[0-9]{7}[A-Z0-9]{1}$|^[T]{1}[A-Z0-9]{8}$)|^[0-9]{8}[A-Z]{1}$)')) {
		return false;
	}

	// Test con letra T
	if (/^[T]{1}/.test(value)) {
		return (value[8] === /^[T]{1}[A-Z0-9]{8}$/.test(value));
	}

	// Comprobamos dígito de control para las letras X,Y y Z
	if (/^[XYZ]{1}/.test(value)) {
		return (value[8] === "TRWAGMYFPDXBNJZSQVHLCKE"
				.charAt(value.replace('X', '0').replace('Y', '1').replace('Z',
						'2').substring(0, 8) % 23));
	}

	return false;
}

function validateEmail(email) { 
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function validateNumbers(inputtxt)  
{  
   var numbers = /^[0-9]+$/;  
   if(inputtxt.value.match(numbers))  
   {  
   alert('Your Registration number has accepted....');  
   document.form1.text1.focus();  
   return true;  
   }  
   else  
   {  
   alert('Please input numeric characters only');  
   document.form1.text1.focus();  
   return false;  
   }  
} 
