<div id="content">
		<div class="contacts-3">
			<div class="container">
			<div class="row">
				<h3>CREACIÓN MANUAL DE USUARIO</h3>
				<h5>**Operación de uso exclusivo del equipo de operaciones de BONOPARK SL**</h5>
				<p>Cualquier uso fraudulento de la aplicación podrá conllevar una sanción</p>
			</div>
				<form name="formulario" id="formulario" action="operaciones.php" method="post" onsubmit="return validarFormulario();" >
					<div class="col-xs-6">
						<fieldset>
							<legend>CADENA DATOS DEL USUARIO</legend>
							
							<label>Ds_MerchantData:</label> 
							<input type="text" class="form-control" name="Ds_MerchantData" id="Ds_MerchantData"> 
							
						</fieldset>
					</div>
					
					<div class="col-xs-6 form-colm-2">
						
						<fieldset>
							<legend>DATOS INTERNOS TPV</legend>
							<label>Ds_Merchant_Identifier (Nº referencia del pago):</label> 
							<input type="text" class="form-control" name="Ds_Merchant_Identifier" id="Ds_Merchant_Identifier"> 
							
							<label>Ds_ExpiryDate (Caducidad tarjeta):</label> 
							<input type="text" class="form-control" name="Ds_ExpiryDate" id="Ds_ExpiryDate">
							 
							<label>Ds_Amount (Importe en centimos):</label> 
							<input type="text" class="form-control" name="Ds_Amount" id="Ds_Amount">
							
							<label>Ds_Response:</label> 
							<input type="text" class="form-control" name="Ds_Response" id="Ds_Response">
						</fieldset>
					</div>
					
					<input type="submit" name="submit" value="CREACION MANUAL DE USUARIO"> 
				</form>
			</div>
		</div>
	</div>