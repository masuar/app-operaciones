<?php
include ('mysql/db_connect.php');

// Controlo el botón que se pulsó en la pantalla anterior
$elsub = $_POST ['submit'];

?>

<!DOCTYPE html>
<html>
<head>
<title>Aplicación Bici - BiciMAD</title>

<meta charset="utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1" />
<link rel="shortcut icon" href="resources/flat-ui/images/favicon.ico" />
<link rel="stylesheet" type="text/css" href="resources/css/animations.css" />
<link rel="stylesheet" type="text/css" href="resources/css/style.css" />
<link rel="stylesheet" type="text/css" href="resources/css/style-crm.css" />
<link rel="stylesheet" type="text/css" href="resources/flat-ui/bootstrap/css/bootstrap.css" />
<link rel="stylesheet" type="text/css" href="resources/flat-ui/css/flat-ui.css" />
<link rel="stylesheet" type="text/css" href="resources/css/icon-font.css" />

<!-- Placed at the end of the document so the pages load faster -->
<script src="http://code.jquery.com/jquery-1.9.1.js"></script>
<script src="http://code.jquery.com/ui/1.10.3/jquery-ui.js"></script>
<script src="resources/flat-ui/js/bootstrap.min.js"></script>
<script src="resources/js/modernizr.custom.js"></script>
<script src="resources/js/easing.min.js"></script>
<script src="resources/js/jquery.svg.js"></script>
<script src="resources/js/jquery.svganim.js"></script>
<script src="resources/js/script.js"></script>

<style>
	input[type="text"]{ width:97% !important; padding-left:1%; padding-bottom:5px; padding-top:5px;}
	input[type="button"]{ width:97% !important;padding-left:1%;padding-bottom:5px; padding-top:5px;}
	input[type="email"]{ width:97%!important;padding-left:1%;padding-bottom:5px; padding-top:5px;}
	.form_settings{ width:50%; float:left;}
	
	@media (max-width: 600px) {
		.form_settings{ width:100%; margin-left:0%; float:left;}
		input[type="text"]{ width:99% !important;}
		input[type="button"]{ width:99% !important;}
		input[type="email"]{ width:99%!important;}
	}

</style>
</head>
<body>
	<div id="header"></div>
	
		<?php
		/* --- Operación CREAR USUARIO ---- */
		if ($elsub == "CREAR USUARIO") {
			include 'operaciones/crear_usuario.php';
		
		/* --- Operación COMPRAR ABONO ---- */
		} else	if ($elsub == "CREACION MANUAL DE USUARIO") {
			include 'operaciones/resumen_registro_usuario.php';
			
		} 
		?>

	<div id="footer"></div>


	<script>
		$("#header").load("common/header.html");
	</script>
</body>
</html>